#ifndef MYSTRUCTS_H
#define MYSTRUCTS_H

#include "../utils/include/strBuffer.h"

// struct used by the expressions
typedef struct structExpr
{
    char *addr;
    int value;

    char *code;
} structExpr;

// structs used by the markers
typedef struct structMarkerCycle
{
    char *beginLabel;
    char *trueLabel;
    char *falseLabel;
} structMarkerCycle;

typedef struct structMarkerIf
{
    char *startIfLabel;
    char *endIfLabel;
    char *startElseLabel;
} structMarkerIf;

#endif
