#ifndef STRBUFFER_H
#define STRBUFFER_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/**
 * DEFINE SECTION
 */
#define SIZE_BUFFER 100

/**
 * This is the struct used by the buffer
 */
typedef struct strBuffer
{
    char *value;
    int size;
} strBuffer;

/**
 * This function initializes a string buffer
 * @param buffer is the buffer to manage
 */
void initBuffer(strBuffer *buffer);

/**
 * This function frees the memory allocated to the buffer
 * @param buffer is the buffer to manage
 */
void deleteBuffer(strBuffer buffer);

/**
 * This function adds a string to the buffer
 * @param buffer is the buffer to manage
 * @param str is the string to insert into the buffer
 */
void addBuffer(strBuffer *buffer, char *str);

#endif
