#ifndef MYUTILS_H
#define MYUTILS_H

#include <stdlib.h>
#include <stdarg.h>

/**
 * DEFINE SECTION
 */
#define SIZE_BUFFER_CREATE_POINTER 1000000
#define DEFAULT_INT 0
#define EMPTY_STR (char *)calloc(1, sizeof(char)) // is our empty string

#define NAME_H_FILE_T "build/hFileT.h"
#define NAME_3AC_FILE "build/3AC.c"
#define OPEN_MODE_FILE "w"

/**
 * Add str to the buffer
 * @param str is the string to save
 */
void saveRule(char *str);

/**
 * This function creates a string using a pattern like printf
 * @param pattern is the pattern used to create the string (e.g. "%s Hi:) %d")
 * @param ... is a list of pointers to params
 * @return a new string allocated in the heap memory
 */
char *createCharPointer(char *pattern, ...);

/**
 * This function checks whether the str ends with a label
 * @param str is the string to check
 * @return 1 if str ends with a label, otherwise 0
 */
int isLabelLastValue(char *str);

/**
 * This function creates the 3AC of a cycle (e.g. for or while)
 * @param beginLabel is the begin label of the cycle
 * @param boolExprCode is the block code of the boolean expression
 * @param boolExprT is the tmp variable used to check the truth of the if condition
 * @param trueLabel is the label of the block code if the condition = true
 * @param falseLabel is the label of the cycle end
 * @param linesCode is the block code of the cycle
 * @param inc is the block code used by the for cycle, where we manage the autoincrement of a variable at the end of the cycle (while -> inc = EMPTY_STR)
 * @return the string representing the 3AC of the cycle
 */
char *createCycle(char *beginLabel, char *boolExprCode, char *boolExprT, char *trueLabel, char *falseLabel, char *linesCode, char *inc);

/**
 * This function creates the 3AC of the if statement
 * @param boolExprCode is the block code of the boolean expression
 * @param boolExprT is the tmp variable used to check the truth of the if condition
 * @param trueLabel is the label of the block code if the condition = true
 * @param falseLabel is the label of the block code if the condition = false
 * @param linesCodeTrue is the block code of the if part
 * @param linesCodeFalse is the block code of the else part
 * @return the string representing the 3AC of the if statement
 */
char *createIf(char *boolExprCode, char *boolExprT, char *trueLabel, char *falseLabel, char *linesCodeTrue, char *linesCodeFalse);

/**
 * This function creates a new label
 * @return a unique identifier for a label
 */
char *newLabel();

/**
 * This function creates a new tmp variable
 * @return a unique identifier for a variable
 */
char *newVar();

/**
 * This functions creates a generic file
 * @param filename is the name of the file
 * @param mode is the file access mode
 * @param str is the string to write into the file
 */
void createFile(char *filename, char *mode, char *str);

/**
 * This function creates the 3AC files
 */
void create3ACFile();

/**
 * This function creates the header file with declarations of tmp variables
 */
void createHFileT();

/**
 * This function initializes the 3AC buffer
 */
void init3ACBuffer();

/**
 * This function deletes the 3AC buffer
 */
void delete3ACBuffer();

/**
 * This function adds a string into the 3AC buffer
 * @param str is the string to add
 */
void add3ACBuffer(char *str);

/**
 * This function prints the 3AC in the console but first it removes some chars that we use to compile correctly
 */
void print3ACBuffer();

#endif
