#include "include/strBuffer.h"

/**
 *  This function initializes a string buffer
 * @param buffer is the buffer to manage
 */
void initBuffer(strBuffer *buffer)
{
    // we create the initial memory area 
    buffer->value = (char *)calloc(SIZE_BUFFER, sizeof(char));
    // size is a counter used to resize the buffer when it's full
    buffer->size = 1;
}

/**
 * This function frees the memory allocated to the buffer
 * @param buffer is the buffer to manage
 */
void deleteBuffer(strBuffer buffer)
{
    free(buffer.value);
}

/**
 * This function adds a string to the buffer
 * @param buffer is the buffer to manage
 * @param str is the string to insert into the buffer
 */
void addBuffer(strBuffer *buffer, char *str)
{
    // +1 needs to store also the '\0' which is the terminal char for a string
    int neededSize = strlen(buffer->value) + strlen(str) + 1;

    if(neededSize >= buffer->size * SIZE_BUFFER)
    {
        // calculate the needed space to resize the buffer
        while(neededSize >= ++buffer->size * SIZE_BUFFER);

        buffer->value = (char *)realloc(buffer->value, buffer->size * SIZE_BUFFER);
        if(buffer->value == NULL)
        {
            printf("\nERROR: Can't realloc memory to the buffer!!!\n");
            exit(-1);
        }
        // printf("\a\nRealloc..[%d]\n", buffer->size);
    }

    // copy the string at the end of the buffer
    strcpy((buffer->value + strlen(buffer->value)), str);
}
