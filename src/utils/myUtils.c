#include "include/strBuffer.h"
#include "include/myUtils.h"

/**
 * GLOBAL VARIABLES SECTION
 */
// number of tmp variables
int numberT = 0;
// number of labels
int numberL = 0;
// buffer of all 3AC
strBuffer buffer3AC;

/**
 * Add str to the buffer
 * @param str is the param to save
 */
void saveRule(char *str)
{
    add3ACBuffer(str);
}

/**
 * This function prints the 3AC in the console but first it removes some chars that we use to compile correctly
 */
void print3ACBuffer()
{
    strBuffer localBuffer;
    // before using the buffer we need to init
    initBuffer(&localBuffer);
    char *c;

    //cleaning the {}; before print
    for(int i = 0; i < (long)strlen(buffer3AC.value) - 1; i++)
    {
        // we remove all the {}
        if(buffer3AC.value[i] != '{' && buffer3AC.value[i] != '}')
        {
            // we remove all the ; that aren't at the end of the line
            if(buffer3AC.value[i] == ';' && buffer3AC.value[i + 1] == '\n')
            {
                c = createCharPointer("%c", buffer3AC.value[i]);
                addBuffer(&localBuffer, c);
                free(c);
            }

            if(buffer3AC.value[i] != ';')
            {
                c = createCharPointer("%c", buffer3AC.value[i]);
                addBuffer(&localBuffer, c);
                free(c);
            }
        }
    }

    printf("%s", localBuffer.value);

    // deleting the allocated memory to the buffer
    deleteBuffer(localBuffer);
}

/**
 * This function creates a string using a pattern like printf
 * @param pattern is the pattern used to create the string (e.g. "%s Hi:) %d")
 * @param ... is a list of pointers to params
 * @return a new string allocated in the heap memory
 */
char *createCharPointer(char *pattern, ...)
{
    int writtenChars;
    va_list ptrArgs;
    char buffer[SIZE_BUFFER_CREATE_POINTER]; // we create a big local area, then we'll optimize the space
    char *str;

    va_start(ptrArgs, pattern);
    // insert the string in the local buffer
    writtenChars = vsprintf(buffer, pattern, ptrArgs);
    // Here we optimize the space, since in the heap memory we create a char * with dimension equals to the length of the just created string
    str = (char *)calloc(strlen(buffer) + 1, sizeof(char));
    strcpy(str, buffer);
    va_end(ptrArgs);

    return str;
}

/**
 * This function creates the 3AC of a cycle (e.g. for or while)
 * @param beginLabel is the begin label of the cycle
 * @param boolExprCode is the block code of the boolean expression
 * @param boolExprT is the tmp variable used to check the truth of the if condition
 * @param trueLabel is the label of the block code if the condition = true
 * @param falseLabel is the label of the cycle end
 * @param linesCode is the block code of the cycle
 * @param inc is the block code used by the for cycle, where we manage the autoincrement of a variable at the end of the cycle (while -> inc = EMPTY_STR)
 * @return the string representing the 3AC of the cycle
 */
char *createCycle(char *beginLabel, char *boolExprCode, char *boolExprT, char *trueLabel, char *falseLabel, char *linesCode, char *inc)
{
    // if linesCode ends with a label we have to add ; in order to be able to compile the 3AC code
    char *lines = isLabelLastValue(linesCode) ? createCharPointer("%s;", linesCode) : strdup(linesCode);

	char * result = createCharPointer("%s: %sif(%s) goto %s;\ngoto %s;\n%s: {%s}%sgoto %s;\n%s: ;",
                                      beginLabel, boolExprCode, boolExprT, trueLabel, falseLabel, trueLabel, lines, inc, beginLabel, falseLabel);

    free(lines);
    return result;
}

/**
 * This function creates the 3AC of the if statement
 * @param boolExprCode is the block code of the boolean expression
 * @param boolExprT is the tmp variable used to check the truth of the if condition
 * @param trueLabel is the label of the block code if the condition = true
 * @param falseLabel is the label of the block code if the condition = false
 * @param linesCodeTrue is the block code of the if part
 * @param linesCodeFalse is the block code of the else part
 * @return the string representing the 3AC of the if statement
 */
char *createIf(char *boolExprCode, char *boolExprT, char *trueLabel, char *falseLabel, char *linesCodeTrue, char *linesCodeFalse)
{
    // if linesCodeTrue or linesCodeFalse end with a label we have to add ; in order to be able to compile the 3AC code
    char *linesTrue = isLabelLastValue(linesCodeTrue) ? createCharPointer("%s;", linesCodeTrue) : strdup(linesCodeTrue);
    char *linesFalse = isLabelLastValue(linesCodeFalse) ? createCharPointer("%s;", linesCodeFalse) : strdup(linesCodeFalse);

    char * result = createCharPointer("%sif(%s) goto %s;\ngoto %s;\n%s: {%s}%s: {%s}",
                                      boolExprCode, boolExprT, trueLabel, falseLabel, trueLabel, linesTrue, falseLabel, linesFalse);

    free(linesTrue);
    free(linesFalse);
    return result;
}

/**
 * This function creates a new label
 * @return a unique identifier for a label
 */
char *newLabel()
{
    return createCharPointer("L%d", numberL++);
}

/**
 * This function creates a new tmp variable
 * @return a unique identifier for a variable
 */
char *newVar()
{
    return createCharPointer("t%d", numberT++);
}

/**
 * This function checks whether the str ends with a label
 * @param str is the param to check
 * @return 1 if str ends with a label, otherwise 0
 */
int isLabelLastValue(char *str)
{
    return strcmp(str + strlen(str) - 2, ": ") == 0;
}

/**
 * This function creates the 3AC files
 */
void create3ACFile()
{
    // first we set the include part and the main()
    char *initFile = "#include <stdio.h>\n#include \"hFileT.h\"\n\nint main(){\n";
    // same trouble with the end of the buffer -> we have to put ; after a label
    // the last } closes the main(){
    char *toPrint = isLabelLastValue(buffer3AC.value) ? createCharPointer("%s%s;}\n", initFile , buffer3AC.value) : createCharPointer("%s%s}\n", initFile , buffer3AC.value);

    // we have to specify the filename, the access mode and the string to write into the file
    createFile(NAME_3AC_FILE, OPEN_MODE_FILE, toPrint);

    free(toPrint);
}

/**
 * This function creates the header file with the declaration of the tmp variables
 */
void createHFileT()
{
    strBuffer buffer;
    char *str;

    initBuffer(&buffer);
    addBuffer(&buffer, "#ifndef MY_H_FILE_T\n#define MY_H_FILE_T\n\n");

    // for each tmp variable we create a int variable declaration to add into the buffer
    for(int i = 0; i < numberT; i++)
    {
        str = createCharPointer("int t%d;\n", i);
        addBuffer(&buffer, str);
        free(str);
    }

    addBuffer(&buffer, "\n#endif\n");
    createFile(NAME_H_FILE_T, OPEN_MODE_FILE, buffer.value);

    deleteBuffer(buffer);
}

/**
 * This functions creates a generic file
 * @param filename is the name of the file
 * @param mode is the file access mode
 * @param str is the string to write into the file
 */
void createFile(char *filename, char *mode, char *str)
{
    FILE *file;

    // open the file
    file = fopen(filename, mode);
    //check if the open operation is done correctly
    if(file == NULL)
    {
        printf("\nERROR: Can't open the file!!!\n");
        exit(-1);
    }

    // writing
    fprintf(file, "%s", str);

    // close the file
    fclose(file);
}

/**
 * This function adds a string into the 3AC buffer
 * @param str is the string to add
 */
void add3ACBuffer(char *str)
{
    addBuffer(&buffer3AC, str);
}

/**
 * This function initializes the 3AC buffer
 */
void init3ACBuffer()
{
    initBuffer(&buffer3AC);
}

/**
 * This function deletes the 3AC buffer
 */
void delete3ACBuffer()
{
    deleteBuffer(buffer3AC);
}
