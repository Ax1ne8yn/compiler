%{
#include "../src/include/myStructs.h"
#include "../src/utils/include/myUtils.h"

/**
 * PROTOTYPE FUNCTIONS SECTION
 */
int yylex();
int yyerror(char *s);

/**
 * GLOBAL VARIABLES SECTION
 */
// string where we'll save the name of a new tmp var
char *t;

// the following counters will be used to save the level number of nested cycles
int countWhileStatement = 0;
int countForStatement = 0;
int countIfStatement = 0;
%}

// the %union declaration specifies the entire collection of possible data types for the parsing tree
// these types are used in %token and %type declarations for a terminal or a nonterminal symbol
%union{
	structExpr valStructExpr;
	structMarkerCycle valstructMarkerCycle;
	structMarkerIf valStructMarkerIf;
}

/**
 * TOKEN SECTION
 */
%token <valStructExpr> NUMBER
%token <valStructExpr> ID
%token <valStructExpr> INC
%token INT PRINT WHILE FOR IF ELSE
%token OPENBRACE CLOSEBRACE OPENPARENT CLOSEPARENT OPADD OPMINUS OPSTAR OPFRACT ENDFILE SEMICOLON

/**
 * TYPE SECTION FOR EXPR
 */
%type <valStructExpr> lines
%type <valStructExpr> expr
%type <valStructExpr> idExpr
%type <valStructExpr> comp
%type <valStructExpr> boolExpr
%type <valStructExpr> createNext
%type <valStructExpr> whileExpr
%type <valStructExpr> linesWhileExpr
%type <valStructExpr> forExpr
%type <valStructExpr> linesForExpr
%type <valStructExpr> ifExpr
%type <valStructExpr> initExprFor
%type <valStructExpr> incExprFor
%type <valStructExpr> elseExpr

/**
 * TYPE SECTION FOR MARKERS
 */
%type <valstructMarkerCycle> markerWhile
%type <valstructMarkerCycle> markerFor
%type <valStructMarkerIf> markerIf

/**
 * PRIORITY-ASSOCIATIVITY SECTION
 */
%left OPADD OPMINUS
%left OPSTAR OPFRACT
%right UMINUS
%left OR
%left AND
%right NOT

/**
 * NON-ASSOCIATIVITY SECTION
 */
%nonassoc EQUAL NOTEQUAL LESSTHAN GREATERTHAN LESSEQUAL GREATEREQUAL 

%%
/**
 * These productions are the first executed by the program
 * It could be a line of code or itself recursively if our program has a lot of lines
 * Each line need a SEMICOLON = ; at the end
 * To finish the program we have to reach the ENDFILE token
 */
program:    lines
            | SEMICOLON
            | program SEMICOLON
            | program ENDFILE           { return 0; }
            | ENDFILE                   { return 0; }
            ;

/**
 * These productions are the most important because it could be "everything"
 *      * forExpr
 *      * ifExpr
 *      * whileExpr
 *      * idExpr
 *      * expr
 *      * INC
 *
 * Each expression is present with its recursive version
 * We also need an another expression -> createNext, which is used by forEpr, whileExpr and ifExpr
 *      * It creates a new label used by the parsed 3AC to return at the end of a cycle or it defines the begin of the else part in a if-else statement
 *
 * A lot of parts are similar, therefore we'll not comment all the code below.
 */
lines:      lines createNext forExpr
            {
                // in the $$.code variable we save the $1.code (lines) from the below levels + the $3.code which is the block code of the for cycle
                $$.code = createCharPointer("%s%s", $1.code, $3.code);

                if(countIfStatement == 0 && countWhileStatement == 0 && countForStatement == 0)
                {
                    // we save code in the 3AC buffer only if we aren't in a nested statement
                    saveRule($3.code);
                }

                free($3.code);
            }
            | createNext forExpr
            {
                // in this case we just save the code of the for cycle
                $$.code = $2.code;

                if(countIfStatement == 0 && countWhileStatement == 0 && countForStatement == 0)
                {
                    saveRule($2.code);
                    free($2.code);
                }
            }
            | lines createNext ifExpr
            {
                $$.code = createCharPointer("%s%s", $1.code, $3.code);

                if(countIfStatement == 0 && countWhileStatement == 0 && countForStatement == 0)
                {
                    saveRule($3.code);
                }

                free($3.code);
            }
            | createNext ifExpr
            {
                $$.code = $2.code;

                if(countIfStatement == 0 && countWhileStatement == 0 && countForStatement == 0)
                {
                    saveRule($2.code);
                    free($2.code);
                }
            }
            | lines createNext whileExpr
            {
                $$.code = createCharPointer("%s%s", $1.code, $3.code);

                if(countWhileStatement == 0 && countIfStatement == 0 && countForStatement == 0)
                {
                    saveRule($3.code);
                }

                free($3.code);
            }
            | createNext whileExpr
            {
                $$.code = $2.code;

                if(countWhileStatement == 0 && countIfStatement == 0 && countForStatement == 0)
                {
                    saveRule($2.code);
                    free($2.code);
                }
            }
            | lines idExpr SEMICOLON
            {
                $$.code = createCharPointer("%s%s", $1.code, $2.code);

                if(countWhileStatement == 0 && countForStatement == 0 && countIfStatement == 0)
                {
                    saveRule($2.code);
                }

                free($2.code);
            }
            | expr SEMICOLON
            {
                // this rule is needed to avoid code like: a; or 5; -> that have no meaning
                // in fact if the expr is a NUMBER or ID the $1.code is equal to EMPTY_STR
                if(strcmp($1.code, EMPTY_STR) == 0)
                {
                    yyerror("syntax error");

                    free($1.code);
                    return -1;
                }
            }
            | idExpr SEMICOLON
            {
                $$.code = $1.code;

                if(countWhileStatement == 0 && countIfStatement == 0 && countForStatement == 0)
                {
                    saveRule($1.code);
                }
            }
            | lines INC SEMICOLON
            {
                // this rule manages lines like: a++;
                char *tmp = createCharPointer("%s = %s + 1;\n", $2.addr, $2.addr);
                $$.code = createCharPointer("%s%s", $1.code, tmp);

                if(countWhileStatement == 0 && countIfStatement == 0 && countForStatement == 0)
                {
                    saveRule(tmp);
                }

                free($2.addr);
                free(tmp);
            }
            | INC SEMICOLON
            {
                char *tmp = createCharPointer("%s = %s + 1;\n", $1.addr, $1.addr);
                $$.code = tmp;

                if(countWhileStatement == 0 && countIfStatement == 0 && countForStatement == 0)
                {
                    saveRule(tmp);
                    free(tmp);
                }

                free($1.addr);
            }
            ;

/**
 * These productions create the 3AC about a for statement
 *
 * For example it could be:
 *      * for(init; boolean expression; increment part){empty or not}
 *
 * In the syntax of a for statement in addition to the standard syntax we have the markerFor and enableIdExprForStatement rules
 *      * markerFor manages the labels and increments the countForStatement
 *      * enableIdExprForStatement decrements the countForStatement
 */
forExpr:    FOR OPENPARENT markerFor initExprFor SEMICOLON boolExpr SEMICOLON incExprFor CLOSEPARENT OPENBRACE linesForExpr CLOSEBRACE enableIdExprForStatement
            {
                // we use createCycle function to create a cycle using properly the params
                char *tmp = createCycle($3.beginLabel, $6.code, $6.addr, $3.trueLabel, $3.falseLabel, $11.code, $8.code);
                $$.code = createCharPointer("%s%s", $4.code, tmp);

                // at the end we free the memory
                free($3.beginLabel);
                free($3.trueLabel);
                free($3.falseLabel);
                free($4.code);
                free($6.code);
                free($6.addr);
                free($8.code);
                free($11.code);
                free(tmp);
            }
            ;

/**
 * These productions manage the block code of a for statement
 * It could contain the lines expression or nothing -> the last permits to create for statement with no lines inside the braces
 */
linesForExpr:
            lines
            {
                $$.code = $1.code;
            }
            |
            {
                $$.code = EMPTY_STR;
            }
            ;

/**
 * These productions manage the increment part of a for statement
 *
 * For example it could be:
 *      * a++
 *      * a = a + d;
 */
incExprFor:
            INC
            {
                $$.code = createCharPointer("%s = %s + 1;\n", $1.addr, $1.addr);

                free($1.addr);
            }
            | ID '=' expr
            {
                $$.code = createCharPointer("%s%s = %s;\n",$3.code, $1.addr, $3.addr);

                free($1.addr);
                free($3.addr);
                free($3.code);
            }
            ;

/**
 * These productions manage the initialization part of a for statement
 *
 * For example it could be:
 *      * int a -> in this case, a is set to DEFAULT_INT
 *      * int a = 8 or int a = g + c - 1
 *      * a = 8 or int a = g + c - 1
 */
initExprFor:
            INT ID
            {
                $$.code = createCharPointer("int %s = %d;\n", $2.addr, DEFAULT_INT);

                free($2.addr);
            }
            | INT ID '=' expr
            {
                $$.code = createCharPointer("%sint %s = %s;\n", $4.code, $2.addr, $4.addr);

                free($2.addr);
                free($4.addr);
                free($4.code);
            }
            | ID '=' expr
            {
                $$.code = createCharPointer("%s%s = %s;\n", $3.code, $1.addr, $3.addr);

                free($1.addr);
                free($3.addr);
                free($3.code);
            }
            ;

/**
 * These productions create the 3AC of a if-else statement
 * Also in this case, as in the for statement we have markerIf and enableIdExprIfStatement
 *
 * For example it could be:
 *      * if(boolean expression){empty or not}
 *      * else {empty or not}
 *
 * The else part of the statement is optional
 */
ifExpr:     IF OPENPARENT markerIf boolExpr CLOSEPARENT OPENBRACE lines CLOSEBRACE elseExpr enableIdExprIfStatement
            {
                // the ternary operator is used to check if the else part is present in the statement
                char *ifBlockCode = (strcmp($9.code, EMPTY_STR) != 0) ? createCharPointer("%sgoto %s;\n", $7.code, $3.startElseLabel) : strdup($7.code);
                char *elseBlockCode = (strcmp($9.code, EMPTY_STR) != 0) ? createCharPointer("%s%s: ", $9.code, $3.startElseLabel) : EMPTY_STR;

                // we use createIf function to create the if-else statement using properly the params
                $$.code = createIf($4.code, $4.addr, $3.startIfLabel, $3.endIfLabel, ifBlockCode, elseBlockCode);

                // at the end we free the memory
                free($3.startIfLabel);
                free($3.endIfLabel);
                free($3.startElseLabel);
                free($4.code);
                free($4.addr);
                free($7.code);
                free($9.code);
                free(ifBlockCode);
                free(elseBlockCode);
            }
            | IF OPENPARENT markerIf boolExpr CLOSEPARENT OPENBRACE CLOSEBRACE elseExpr enableIdExprIfStatement
            {
                // in this case the if part is empty, therefore we just have to check if the else part is empty or not
                char *ifBlockCode = EMPTY_STR;
                char *elseBlockCode = (strcmp($8.code, EMPTY_STR) != 0) ? strdup($8.code) : EMPTY_STR;

                $$.code = createIf($4.code, $4.addr, $3.startElseLabel, $3.endIfLabel, ifBlockCode, elseBlockCode);

                free($3.startIfLabel);
                free($3.endIfLabel);
                free($3.startElseLabel);
                free($4.code);
                free($4.addr);
                free($8.code);
                free(ifBlockCode);
                free(elseBlockCode);
            }
            ;

/**
 * These productions could generate the else part (empty or not) or make it optional
 */
elseExpr:   ELSE OPENBRACE lines CLOSEBRACE
            {
                $$.code = $3.code;
            }
            | ELSE OPENBRACE CLOSEBRACE
            {
                $$.code = EMPTY_STR;
            }
            |
            {
                $$.code = EMPTY_STR;
            }
            ;

/**
 * This production creates the 3AC for a while cycle
 * Also in this case, as in the for statement we have markerWhile and enableIdExprWhileStatement
 *
 * For example it could be:
 *      * while(boolean expression){empty or not}
 */
whileExpr:  WHILE OPENPARENT markerWhile boolExpr CLOSEPARENT OPENBRACE linesWhileExpr CLOSEBRACE enableIdExprWhileStatement
            {
                // we use createCycle function to create a cycle using properly the params
                $$.code = createCycle($3.beginLabel, $4.code, $4.addr, $3.trueLabel, $3.falseLabel, $7.code, EMPTY_STR);

                // at the end we free the memory
                free($3.beginLabel);
                free($3.trueLabel);
                free($3.falseLabel);
                free($4.code);
                free($4.addr);
                free($7.code);
            }
            ;

/**
 * These productions manage the block code of a while statement
 * It could contain the lines expression or nothing -> the last permits to create while statement with no lines inside the braces
 */
linesWhileExpr:
            lines
            {
                $$.code = $1.code;
            }
            |
            {
                $$.code = EMPTY_STR;
            }
            ;

/**
 * This production decrements the counter of the if statement
 */
enableIdExprIfStatement:
            {
                countIfStatement--;
            }
            ;

/**
 * This production decrements the counter of the for statement
 */
enableIdExprForStatement:
            {
                countForStatement--;
            }
            ;

/**
 * This production decrements the counter of the while statement
 */
enableIdExprWhileStatement:
            {
                countWhileStatement--;
            }
            ;

/**
 * This production creates a new label
 */
createNext:
            {
                $$.code = newLabel();
            }
            ;

/**
 * This production manages the labels and increments the countIfStatement
 */
markerIf:
            {
                countIfStatement++;
                $$.startIfLabel = newLabel();
                $$.endIfLabel = newLabel();
                // we take this label 2 position back in the stack -> it has been inserted by the createNext expression
                $$.startElseLabel = $<valStructExpr>-2.code;
            }
            ;

/**
 * This production manages the labels and increments the countForStatement
 */
markerFor:
            {
                countForStatement++;
                $$.beginLabel = newLabel();
                $$.trueLabel = newLabel();
                // we take this label 2 position back in the stack -> it has been inserted by the createNext expression
                $$.falseLabel = $<valStructExpr>-2.code;
            }
            ;

/**
 * This production manages the labels and increments the countWhileStatement
 */
markerWhile:
            {
                countWhileStatement++;
                $$.beginLabel = newLabel();
                $$.trueLabel = newLabel();
                // we take this label 2 position back in the stack -> it has been inserted by the createNext expression
                $$.falseLabel = $<valStructExpr>-2.code;
            }
            ;

/**
 * These productions manage the identifier of the variables and the print function
 *
 * For example it could be:
 *      * int a -> in this case, a is set to DEFAULT_INT
 *      * int a = 8 or int a = g + c - 1
 *      * a = 8 or int a = g + c - 1
 *      * a = a < b
 *      * print(10)
 *      * print(a)
 *      * print(a + 1)
 *      * print(a++)
 */
idExpr:     INT ID
            {
                $$.code = createCharPointer("int %s = %d;\n", $2.addr, DEFAULT_INT);

                free($2.addr);
            }
            | INT ID '=' expr
            {
                $$.code = createCharPointer("%sint %s = %s;\n", $4.code, $2.addr, $4.addr);

                 free($2.addr);
                 free($4.addr);
                 free($4.code);
            }
            | ID '=' expr
            {
                $$.code = createCharPointer("%s%s = %s;\n",$3.code, $1.addr, $3.addr);

                free($1.addr);
                free($3.addr);
                free($3.code);
            }
            | ID '=' boolExpr
            {
                $$.code = createCharPointer("%s%s = %s;\n",$3.code, $1.addr, $3.addr);

                free($1.addr);
                free($3.addr);
                free($3.code);
            }
            | PRINT OPENPARENT expr CLOSEPARENT
            {
                $$.code = createCharPointer("%sprintf(\"%%d\\n\", %s);\n", $3.code, $3.addr);

                free($3.addr);
                free($3.code);
            }
            | PRINT OPENPARENT INC CLOSEPARENT
            {
                $$.code = createCharPointer("printf(\"%%d\\n\", %s);\n%s = %s + 1;\n", $3.addr, $3.addr, $3.addr);

                free($3.addr);
            }
            ;

/**
 * These productions manage all the possible operations between boolean expressions
 * The syntax supports also the () -> this means that we manage the priority between operands
 * A boolExpr could be also a comparison between expressions
 */
boolExpr:   boolExpr OR boolExpr
            {
                // create a new tmp variable
                t = newVar();
                // saving the code about the OR operation (in this case)
                $$.code = createCharPointer("%s%s%s = %s || %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
                // save the variable which will be used in the above level
				$$.addr = t;

                // at the end we free the memory
				free($1.addr);
				free($1.code);
				free($3.addr);
				free($3.code);

            }
			| boolExpr AND boolExpr
			{
			    t = newVar();
                $$.code = createCharPointer("%s%s%s = %s && %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
		        $$.addr = t;

		        free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
			| NOT boolExpr
			{
		        t = newVar();
		        $$.code = createCharPointer("%s%s = !%s;\n", $2.code, t, $2.addr);
				$$.addr = t;

				free($2.addr);
                free($2.code);
			}
			| OPENPARENT boolExpr CLOSEPARENT
			{
			    $$.code = $2.code;
			    $$.addr = $2.addr;
			}
			| comp
			;

/**
 *  These productions manage the comparison between expressions
 */
comp:       expr LESSTHAN expr
            {
                t = newVar();
                $$.code = createCharPointer("%s%s%s = %s < %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

				free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
		    | expr LESSEQUAL expr
		    {
		        t = newVar();
		        $$.code = createCharPointer("%s%s%s = %s <= %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
		    | expr GREATEREQUAL expr
		    {
		        t = newVar();
		        $$.code = createCharPointer("%s%s%s = %s >= %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
		    | expr GREATERTHAN expr
		    {
		        t = newVar();
		        $$.code = createCharPointer("%s%s%s = %s > %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
		    | expr EQUAL expr
		    {
		        t = newVar();
		        $$.code = createCharPointer("%s%s%s = %s == %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
			    $$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
		    }
		    | expr NOTEQUAL expr
		    {
		        t = newVar();
		        $$.code = createCharPointer("%s%s%s = %s != %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
		    ;

/**
 * These productions manage operations between expressions
 * The syntax supports also the () -> this means that we manage the priority between operands
 * Another feature of these productions is the ability to change sign in front of an expr
 * It could be also a token -> ID or NUMBER
 */
expr:       expr OPADD expr
            {
                t = newVar();
                $$.code = createCharPointer("%s%s%s = %s + %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
			    $$.addr = t;

			    free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
		    }
	        | expr OPMINUS expr
	        {
	            t = newVar();
	            $$.code = createCharPointer("%s%s%s = %s - %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

                free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
		    }
	        | expr OPSTAR expr
	        {
	            t = newVar();
	            $$.code = createCharPointer("%s%s%s = %s * %s;\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

				free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
	        | expr OPFRACT expr
	        {
	            t = newVar();
                $$.code = createCharPointer("%s%s%s = (int)(%s / %s);\n", $1.code, $3.code, t, $1.addr, $3.addr);
				$$.addr = t;

				free($1.addr);
                free($1.code);
                free($3.addr);
                free($3.code);
			}
	        | OPENPARENT expr CLOSEPARENT
	        {
		        $$.addr = $2.addr;
		        $$.code = $2.code;
			}
	        | OPMINUS expr %prec UMINUS
	        {
	            t = newVar();
	            $$.code = createCharPointer("%s%s = -%s;\n", $2.code, t, $2.addr);
				$$.addr = t;

                free($2.addr);
                free($2.code);
			}
			| ID
			{
			    // in this case we save only the name of the id in the addr field
			    $$.addr = $1.addr;
			    // $$.code = EMPTY_STR -> is used to check if we have: a; or 2; -> see -> expr SEMICOLON in the lines expression
			    $$.code = EMPTY_STR;
			}
	        | NUMBER
	        {
	            // in this case we save only the value of the number in the addr field
				$$.addr = createCharPointer("%d", $1.value);
				// $$.code = EMPTY_STR -> is used to check if we have: a; or 2; -> see -> expr SEMICOLON in the lines expression
				$$.code = EMPTY_STR;
		    }
	        ;

%%
int main()
{
    // we have to init the 3AC buffer
    init3ACBuffer();

    // if during the parsing we have an error -> print the error message and exit from the program
    if(yyparse() != 0)
    {
	    fprintf(stderr, "Abnormal exit\n");
	    return -1;
	}

    // create the 3AC file and the header file with the declaration of the tmp variables
    createHFileT();
    create3ACFile();

    // print the 3AC on the console
    print3ACBuffer();
	
    // deleting the buffer
    delete3ACBuffer();

	return 0;
}

/**
 * This function writes an error message in the stderr buffer 
 * @param error is the message to print
 */
int yyerror(char *error)
{
    fprintf(stderr, "ERROR: %s\n", error);
}
