#!/usr/bin/env bash
rm -r C:/Users/Fabio/Documents/compiler/build
mkdir build
bison -obuild/SyntaxAnalyzer.tab.c -d src/SyntaxAnalyzer.y
flex -obuild/LexicalAnalyzer.yy.c src/LexicalAnalyzer.fl
gcc -std=gnu99 -obuild/Parser src/utils/strBuffer.c src/utils/myUtils.c build/SyntaxAnalyzer.tab.c build/LexicalAnalyzer.yy.c -L "C:\GnuWin32\lib" -lfl
build/Parser < src/test/TestExpressions12.c
gcc -std=gnu99 -obuild/3AC build/3AC.c
echo -e "\n\n3AC executed output: \n"
build/3AC
echo -e "\n"