#!/usr/bin/env bash

mkdir -p build
bison -o build/SyntaxAnalyzer.tab.c -d src/SyntaxAnalyzer.y
flex -o build/LexicalAnalyzer.yy.c src/LexicalAnalyzer.fl
gcc -std=gnu99 -o build/Parser src/utils/strBuffer.c src/utils/myUtils.c build/SyntaxAnalyzer.tab.c build/LexicalAnalyzer.yy.c -lfl
#valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose build/Parser
build/Parser < src/test/TestExpressions10.c
gcc -o build/3AC build/3AC.c
echo -e "\n\n3AC executed output: \n"
build/3AC
echo -e "\n"